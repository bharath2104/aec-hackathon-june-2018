package com.example.bharathchandragurrala.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ApplyActivity extends AppCompatActivity {

    private Button applyActivityToQualificationsQuestionnaireBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply);
        loadView();
    }

    private void loadView(){

        applyActivityToQualificationsQuestionnaireBtn = (Button)findViewById(R.id.applyActivityToQualificationsQuestionnaire);

        applyActivityToQualificationsQuestionnaireBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ApplyActivity.this, "ExperienceQuestionnareActivity Clicked", Toast.LENGTH_SHORT).show();
                Uri uri = Uri.parse("https://console.firebase.google.com/project/carpediem-9faf/storage/carpediem-9faf.appspot.com/files~2FTrainingData~2FPainterData~2F"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                //Navigate to Detail Activity
                //Intent intent = new Intent(ApplyActivity.this, DetailActivity.class);
                //startActivity(intent);

            }
        });
    }
}
